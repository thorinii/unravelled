# Unravelled

A Python script to extract the newest N items from a Raindrop.io folder, remove them, and send a digest email containing them.

## Development

```bash
python3 -mvenv .venv
. .venv/bin/activate
pip install --no-binary unravelled .
```
