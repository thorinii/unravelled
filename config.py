# Your API Key for raindrop
raindrop_apikey = "..."

# The name of the Raindrop collection you want to unravel
collection_name = "1 INBOX"

# How many items to send in a digest (at most) in one go
items_to_send = 8

# The address to send the digest email to
to_address = "..."

# The address in the "From" line of the email
from_address = "unravelled@..."

# The base URL for the Raindrop.io API server
raindrop_url = "https://api.raindrop.io/rest"

# The hostname/ip-address for the SMTP server
smtp_server = "localhost"
