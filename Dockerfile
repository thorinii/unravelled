FROM python:3.10-slim

WORKDIR /app

COPY config.py \
     main.py \
     README.md \
     setup.py \
     /app/

RUN pip install .

CMD [ "python", "main.py" ]
