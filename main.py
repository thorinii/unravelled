from datetime import datetime, timedelta, timezone
from email.message import EmailMessage
import html
import smtplib
from typing import List, NamedTuple, Optional

import iso8601
import requests

import config
import setup


class Item(NamedTuple):
    id: int
    url: str
    title: str
    description: Optional[str]
    image_url: Optional[str]
    created_at: datetime


def main() -> None:
    print(f"Unravelled {setup.version}")

    collection_id = find_collection_by_name(config.collection_name)
    if collection_id is not None:
        print(f"Resolved collection to ID {collection_id}")
    else:
        print(f"Failed to resolve collection by name {config.collection_name}")
        return

    top_items = get_top_items(collection_id, config.items_to_send)
    if top_items:
        print(f"Found {len(top_items)} items")
    else:
        print(f"Didn't find any items in {config.collection_name}")
        return

    send_digest_email(config.from_address, config.to_address, top_items)
    print(f"Sent digest email")

    delete_items_from_raindrop(collection_id, top_items)
    print(f"Deleted items from Raindrop")


def find_collection_by_name(name: str) -> Optional[int]:
    response = requests.get(
        f"{config.raindrop_url}/v1/collections",
        headers={"Authorization": f"Bearer {config.raindrop_apikey}"},
    )
    response.raise_for_status()
    result = response.json()
    if not result["result"]:
        return None

    for collection in result["items"]:
        if collection["title"] == name:
            return collection["_id"]

    return None


def get_top_items(collection_id: int, limit: int) -> List[Item]:
    response = requests.get(
        f"{config.raindrop_url}/v1/raindrops/{collection_id}",
        params={"sort": "-created", "perpage": min(limit, 50)},
        headers={"Authorization": f"Bearer {config.raindrop_apikey}"},
    )
    response.raise_for_status()
    result = response.json()
    if not result["result"]:
        return None

    return [
        Item(
            id=item["_id"],
            url=item["link"],
            title=item["title"] or "Unknown",
            description=item["excerpt"] or None,
            image_url=item["cover"] or None,
            created_at=iso8601.parse_date(item["created"]),
        )
        for item in result["items"]
    ]


def send_digest_email(from_address: str, to_address: str, items: List[Item]) -> None:
    message = format_message(items)
    send_email(from_address, to_address, message)


def delete_items_from_raindrop(collection_id: int, items: List[Item]) -> None:
    response = requests.delete(
        f"{config.raindrop_url}/v1/raindrops/{collection_id}",
        json={"ids": [i.id for i in items]},
        headers={"Authorization": f"Bearer {config.raindrop_apikey}"},
    )
    response.raise_for_status()


def format_message(items: List[Item]) -> str:
    now = datetime.today().astimezone(timezone.utc)
    end_of_day = datetime.today().replace(hour=23, minute=59).astimezone(timezone.utc)

    def format_date(date: datetime) -> str:
        day = timedelta(days=1)
        days_ago = int((end_of_day - date) / day)  # truncate, not round
        if days_ago > 7:
            return date.strftime("%a %Y-%m-%d")
        else:
            return f"{date.strftime('%a')}, {days_ago} days ago"

    def format_link(item: Item, text: str) -> str:
        return f'<a href="{e(item.url)}">{e(text)}</a>'

    d = format_date
    e = html.escape
    l = format_link

    formatted_items = [
        f"""
        <h2>{l(i, i.title)}</h2>
        <i>{l(i, i.url)} {d(i.created_at)}</i>
        <p>{e(i.description or "---")}</p>
        """
        for i in items
    ]
    header = "<h1>Unravelled Digest</h1>"
    body = "".join(formatted_items)
    footer = f"<p><small>Generated {e(now.isoformat('T'))}</small></p>"
    return "\n".join([header, body, footer])


def send_email(from_address: str, to_address: str, message: str) -> None:
    msg = EmailMessage()
    msg.set_content(message, subtype="html")

    msg["Subject"] = "Unravelled Digest"
    msg["From"] = from_address
    msg["To"] = to_address

    s = smtplib.SMTP("localhost")
    s.send_message(msg)
    s.quit()


if __name__ == "__main__":
    main()
