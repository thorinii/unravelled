version = "1.0.0"

if __name__ == "__main__":
    from setuptools import setup

    setup(
        name="unravelled",
        version=version,
        py_modules=["main", "config"],
        install_requires=[
            "iso8601 < 2",
            "requests < 3",
        ],
    )
